# Login Form with MySQL

If you want to develop a piece of sowftware and want to use a login system, you might want to use **MySQL** combined with **PHP**, please follow carefully the following instalation guide, ok?

**WARNING:** The PHP script and also the VB.NET project has minimal or none security measures.

## Requirements

 - At least PHP version 5.2
 - You will need [this](https://gitlab.com/treisudest3000/login-form/-/blob/master/LoginForm/PHP%20Script/check.php) file
 - MySQL installed on your server

**NOTE:** The PHP script contains JSONs and comments in romanian, also the VB.NET project contains strings and comments in romanian.