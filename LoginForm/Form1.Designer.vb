﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TexBoxUser = New System.Windows.Forms.TextBox()
        Me.LabelUser = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LabelParola = New System.Windows.Forms.Label()
        Me.TextBoxParola = New System.Windows.Forms.TextBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(211, 115)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(204, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Conectare"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TexBoxUser
        '
        Me.TexBoxUser.Location = New System.Drawing.Point(111, 49)
        Me.TexBoxUser.Name = "TexBoxUser"
        Me.TexBoxUser.Size = New System.Drawing.Size(304, 20)
        Me.TexBoxUser.TabIndex = 1
        '
        'LabelUser
        '
        Me.LabelUser.AutoSize = True
        Me.LabelUser.Location = New System.Drawing.Point(12, 52)
        Me.LabelUser.Name = "LabelUser"
        Me.LabelUser.Size = New System.Drawing.Size(94, 13)
        Me.LabelUser.TabIndex = 2
        Me.LabelUser.Text = "Nume de utilizator:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Segoe Print", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, -1)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(142, 47)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "BadHook"
        '
        'LabelParola
        '
        Me.LabelParola.AutoSize = True
        Me.LabelParola.Location = New System.Drawing.Point(13, 92)
        Me.LabelParola.Name = "LabelParola"
        Me.LabelParola.Size = New System.Drawing.Size(40, 13)
        Me.LabelParola.TabIndex = 4
        Me.LabelParola.Text = "Parola:"
        '
        'TextBoxParola
        '
        Me.TextBoxParola.Location = New System.Drawing.Point(59, 89)
        Me.TextBoxParola.Name = "TextBoxParola"
        Me.TextBoxParola.PasswordChar = Global.Microsoft.VisualBasic.ChrW(35)
        Me.TextBoxParola.Size = New System.Drawing.Size(356, 20)
        Me.TextBoxParola.TabIndex = 5
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(13, 120)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(93, 13)
        Me.LinkLabel1.TabIndex = 6
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "by JPN @ OpClan"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(427, 149)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.TextBoxParola)
        Me.Controls.Add(Me.LabelParola)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LabelUser)
        Me.Controls.Add(Me.TexBoxUser)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Form1"
        Me.Text = "BadHook | Login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents TexBoxUser As TextBox
    Friend WithEvents LabelUser As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents LabelParola As Label
    Friend WithEvents TextBoxParola As TextBox
    Friend WithEvents LinkLabel1 As LinkLabel
End Class
