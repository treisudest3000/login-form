<?php
/*
PHP MySQL Database account lookup by JPN
*/

// Detalii baza date
$server = "localhost";
$utilizator = "root";
$parola = "";
$baza = "checker";

/*
 Parametrii, de exemplu: https://example.com/check.php?user=boss&parola=cGFyb2xhX2luX2Jhc2U2NA==
                                                            ^^^^        ^^^^^^^^^^^^^^^^^^^^^^^^
                                                            user        parola criptata in base64
*/
$utilizator_checker = $_GET["user"];
$parola_checker = $_GET["parola"];

if (!empty($utilizator_checker)) {
    if (!empty($parola_checker)) {
        $conexiune = new mysqli($server, $utilizator, $parola, $baza);
        if ($conexiune->connect_error) {
            die(json_encode(array("return"=>"eroare", "data"=>$conexiune->connect_error)));
        }
        $parola_decoded = base64_decode($parola_checker);
        $comanda = "SELECT * FROM `users` WHERE `username` = '" . $utilizator_checker . "' AND `parola` = '" . $parola_decoded . "'";
        $rezultat = $conexiune->query($comanda);
        if ($rezultat->num_rows > 0) {
            echo(json_encode(array("return"=>"conectat", "data"=>"Te-ai conectat cu succes!")));
        } else {
            die(json_encode(array("return"=>"eroare", "data"=>"Parola sau numele grestie!")));
        }
    } else {
        die(json_encode(array("return"=>"eroare", "data"=>"Parola nu poate fi goala!")));
    }
} else {
    die(json_encode(array("return"=>"eroare", "data"=>"Numele nu poate fi gol!")));
}
?>